import { Component } from '@angular/core';
import { AngularFireAuth } from "angularfire2/auth";
import { AngularFireDatabase, AngularFireList, AngularFireObject } from 'angularfire2/database';
import { Observable } from "rxjs-compat";
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  title = 'Lachat';
  name = 'anonymous';
  phone = "";
  msgVal: string = "";
  usertype: string;

  user: { email: string, password: string } = { email: "", password: "" };
  isLoggedIn: boolean;
  resultMessage: string;

  messages: Observable<any[]>;
  itemRef: AngularFireList<any>;

  constructor(public db: AngularFireDatabase, public afAuth: AngularFireAuth) {

    //this.messages = db.collection('messages').valueChanges();
    this.messages = this.getMessages();

    this.itemRef = this.db.list("messages");
  }

  public setUsertype(usertype: string) {
    this.usertype = usertype;
  }

  getMessages() {
    return this.db.list('messages').valueChanges();
  }

  sendMessage(message: string) {

    this.itemRef.push({ message: message, name: this.name });
    this.msgVal = '';
  }

  login(usertype: string) {
    if (usertype == "admin") {
      this.afAuth.auth.signInWithEmailAndPassword(this.user.email, this.user.password).then(res => {
        console.log(res);

        this.isLoggedIn = true;
        this.name = "admin";
      }, err => {
        console.log(err);
        this.resultMessage = err.message;
      });
    } else {
      if (this.name != "anonymous")
        this.isLoggedIn = true;
      console.log(this.name);
    }

  }

  signup() {
    this.afAuth.auth.createUserWithEmailAndPassword(this.user.email, this.user.password);
  }

}
