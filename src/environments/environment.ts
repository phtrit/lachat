// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAjTNi4IYltUqnBRDfmLpQ1FNpLzMIBl6E",
    authDomain: "lachat-one.firebaseapp.com",
    databaseURL: "https://lachat-one.firebaseio.com",
    projectId: "lachat-one",
    storageBucket: "lachat-one.appspot.com",
    messagingSenderId: "171032114154"
  }
};
